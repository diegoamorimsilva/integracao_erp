<?php

require_once __DIR__ . "/../../../../../../wp-load.php";
require __DIR__ . '/../../../../../../wp-admin/includes/image.php';
require __DIR__ . '/vendor/autoload.php';

require __DIR__ . '/requests.php';
require __DIR__ . '/authenticate.php';
require __DIR__ . '/product.php';


define("URL_RP", 'http://177.124.138.120:9000');
define("URL_WOO", 'https://mercadoinbox.local');
define("CNPJ", "83402711000110");


$auth = new authenticate(
    URL_RP,
    '100051',
    '102030',
    URL_WOO,
    'ck_4ff5d1d8bbd5e39e1861e49875843fead408e5c1',
    'cs_7a01bada40e2359923fff3f98c4990eedf39a93a'
);

$woocommerce = $auth->getWoocommerce();

$integra = new product(URL_RP, '$auth->getToken()', CNPJ);
$integra->create_products_simples($integra,$woocommerce);
$integra->create_products_variable($integra,$woocommerce);
$auth->logout(URL_RP);