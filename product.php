<?php

require_once __DIR__ . "/../../../../../../wp-load.php";

class  product
{
    private $products = array();
    private $chosen_products = array();
    private $simple_products = array();
    private $variable_products = array();
    private $url;
    private $token;
    private $cnpj;

    /**
     *Pegar produtos do Rp Services
     *@param  string $url       URL API RP
     * @param string $token     Token RP
     */
    public function __construct(string $url, string $token, string $cnpj)
    {
        $this->url = $url;
        $this->token = $token;
        $this->cnpj = $cnpj;
    }

    private function rp_pull_all_products()
    {
        $lastId = 0;
        $flag = true;
        $version = 0;
        $total = 0;
        $all_products = array();
        while ($flag) {
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $this->url . '/v2.1/produtounidade/listaprodutos/' . $lastId . '/unidade/' . $this->cnpj . '/detalhado',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_SSL_VERIFYPEER => 0,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_HTTPHEADER => array('token:' . $this->token),
            ));

            $response = curl_exec($curl);

            curl_close($curl);
            $json_produtos = json_decode($response, true);

            if (empty($json_produtos["response"]["produtos"])) {
                $flag = false;
            }

            foreach ($json_produtos["response"]["produtos"] as $produto) {
                $lastId = $produto["Codigo"];
                $all_products[] = $produto;
            }

            echo "\n\nProdutos obtidos: " . $total . "\n";
        }
        $this->products = $all_products;
        return true;
    }

    /**
     * @return array
     */
    public function rp_get_products()
    {
        return $this->rp_pull_all_products();
    }


    /**
     * @return array
     */
    public function rp_get_chosen_products()
    {
        if (empty($this->products)) {
            echo "\nPegue todos os produtos primeiro! \n";
            return null;
        }
        foreach ($this->products as $produto) {
            if ($produto["SetorLoja"] === "FAZPEDIDO") {
                if ($produto["SetorLoja"] === "Padaria Terceiros") {
                    continue;
                }
                if ($produto["Departamento"] === "Hortifruti") {
                    continue;
                }
                if ($produto["Departamento"] === "Padaria Produção Propria") {
                    continue;
                }
                if ($produto["Departamento"] === "Pereciveis") {
                    continue;
                }
                if ($produto["Departamento"] === "Acougue") {
                    continue;
                }

                $chosen_products[] = $produto;
                if ($produto["CodigoPreco"] == "0") {
                    $simple_products[] = $produto;
                } else {
                    $variable_products[] = $produto;
                }
            }
        }


        $data = json_encode($chosen_products);
        file_put_contents('mi_all_products.json', $data);
        $data = json_encode($simple_products);
        file_put_contents('mi_simple_products.json', $data);
        $data = json_encode($variable_products);
        file_put_contents('mi_variable_products.json', $data);
        return true;
    }
    function rp_simple_product()
    {
        if (empty($this->simple_products)) {
            echo "\nArquivos carregados! \n";
            $response = file_get_contents('mi_simple_products.json');
            return json_decode($response, true);
        }
        return $this->simple_products;
    }

    function rp_variable_product()
    {
        if (empty($this->variable_products)) {
            echo "\nPegue os produtos escolhidos  primeiro! \n";
            $response = file_get_contents('mi_variable_products.json');
            return json_decode($response, true);
        }
        return $this->variable_products;
    }

    function update_product_simple_duplicate($e, $produto, $woocommerce)
    {

        $flag = 0;
        $logger = wc_get_logger();
        $skuUp = (string)$produto["Codigo"];
        $precoNovo = (string)$produto["Preco"];

        $productId = wc_get_product_id_by_sku($skuUp);

        $productInMercadoInBox = wc_get_product($productId);
        $precoAtual = $productInMercadoInBox->get_price();
        $idAtual = $productInMercadoInBox->get_id();
        $codNovo = (string) $produto["CodigoBarras"];

        if ((string)$productInMercadoInBox->get_price() !== (string)$produto["Preco"]) {

            $dataUpdate = [
                'regular_price' => (string)$produto["Preco"]

            ];

            try {
                $woocommerce->put('products/' . $productId, $dataUpdate);
                $flag = 1;
            } catch (Exception $ex) {

                $ex = $ex->getMessage();

                $logger->add('MiB_sync_logs', "Ex capturada $ex");

                $flag = 2;
            }
        } else {
            $flag = 3;
        }


        return $flag;
    }

    function create_products_simples($integra, $woocommerce)
    {
        foreach ($integra->rp_simple_product() as $produto) {

            $data = [
                'name' => $produto["Descricao"] . ' ' . $produto["Complemento"],
                'type' => 'simple',
                'sku' => (string)$produto["Codigo"],
                'regular_price' => (string)$produto["Preco"],
                'manage_stock' => true,
                'stock_quantity' => $produto["Estoque1"],
                'grouped_products' => [
                    $produto["Grupo"]
                ],
                'attributes' => [
                    [
                        'name' => 'Marca',
                        'position' => 0,
                        'visible' => true,
                        'variation' => false,
                        'options' => [
                            (string) $produto["Marca"],

                        ]
                    ],
                    [
                        'name' => 'EAN',
                        'position' => 0,
                        'visible' => true,
                        'variation' => false,
                        'options' => [
                            (string) $produto["CodigoBarras"],
                        ]
                    ],

                ],

                'categories' => [
                    [
                        'id' => $this->getCategory($produto["Grupo"])
                    ]
                ],
            ];
            $product = null;
            $logger = wc_get_logger();


            try {
                $product = $woocommerce->post('products', $data);
                // print_r($woocommerce->post('products/categories', $data));
                // print_r($product);
            } catch (Exception $e) {
                $skuUp = (string)$produto["Codigo"];
                $preco = (string)$produto["Preco"];
                $message = $e->getMessage();
                $pos = strpos($message, '[product_invalid_sku]');
                if ($pos !== false) {
                    $resultFunction = $this->update_product_simple_duplicate($e, $produto, $woocommerce);

                    if ($resultFunction === 1) {
                        $logger->add('MiB_sync_logs', "Função update: Atualizou o produto com o SKU: $skuUp");
                    } elseif ($resultFunction === 2) {
                        $logger->add('MiB_sync_logs', "Função update: Erro na atualização do produto com o SKU: $skuUp");
                    } elseif ($resultFunction === 3) {
                        $logger->add('MiB_sync_logs', "Função update: Não precisou atualizar o produto com o SKU: $skuUp");
                    }
                }
                continue;
            }

            if (!is_null($product)) {
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, "https://cdn-cosmos.bluesoft.com.br/products/{$produto["CodigoBarras"]}");
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');

                $image = curl_exec($ch);
                $responseCode = curl_getinfo($ch)['http_code'];
                curl_close($ch);

                if (200 !== $responseCode) {
                    print_r("\n\nImage does not exists. Skipping... (URL) -> https://cdn-cosmos.bluesoft.com.br/products/{$produto["CodigoBarras"]} \n\n");
                    $logger->add('MiB_sync_logs', "Fail. Image does not exists.");
                    continue;
                }

                $finfo = new finfo(FILEINFO_MIME_TYPE);
                $mime = $finfo->buffer($image);

                $filename = (string)$produto["CodigoBarras"] . '.' . str_replace('image/', '', $mime);
                $uploaded_file = wp_upload_bits($filename, null, $image);

                if (!$uploaded_file['error']) {
                    $wp_filetype = wp_check_filetype($filename, null);
                    $attachment  = array(
                        'post_mime_type' => $wp_filetype['type'],
                        'post_parent'    => $product->id,
                        'post_title'     => preg_replace('/\.[^.]+$/', '', $filename),
                        'post_content'   => '',
                        'post_status'    => 'inherit'
                    );

                    $attachment_id = wp_insert_attachment($attachment, $uploaded_file['file'], $product->id);

                    if (!is_wp_error($attachment_id)) {
                        $attachment_data  = wp_generate_attachment_metadata($attachment_id, $uploaded_file['file']);

                        wp_update_attachment_metadata($attachment_id, $attachment_data);

                        set_post_thumbnail($product->id, $attachment_id);

                        update_post_meta($product->id, '_product_image_gallery', $attachment_id);

                        print_r("\n\nImage created successfully\n\n");
                    } else {
                        print($attachment_id->get_error_message() . PHP_EOL);
                        print(' attachment_id is wp error' . PHP_EOL);
                    }
                } else {
                    print('Error when uploading image: ' . $produto["CodigoBarras"] . ' for product ' . $product->id . PHP_EOL);

                    print($uploaded_file['error']);
                }
            }
        }
    }

    function create_products_variable($integra, $woocommerce)
    {
        $logger = wc_get_logger();
        foreach ($integra->rp_variable_product() as $produto) {
            $des =  $produto["Descricao"];
            $comp = $produto["Complemento"];
            $logger->add('MiB_sync_logs', "Desc: $des -- Complemento: $comp");
            $data = [

                'name' => $produto["Descricao"],
                'type' => 'variable',
                'sku' => (string)$produto["CodigoPreco"],
                'regular_price' => (string)$produto["Preco"],
                'description' => $produto["Descricao"],
                'attributes' => [
                    [
                        'name' => 'Marca',
                        'position' => 0,
                        'visible' => true,
                        'variation' => false,
                        'options' => [
                            (string) $produto["Marca"],
                        ]
                    ],
                    [
                        'name' => 'EAN',
                        'position' => 0,
                        'visible' => true,
                        'variation' => false,
                        'options' => [
                            (string) $produto["CodigoBarras"],

                        ]
                    ],
                    [
                        'name' => 'Sabor',
                        'position' => 0,
                        'visible' => true,
                        'variation' => true,
                        'options' => [
                            'Sabor'
                        ]
                    ],

                ],

                'categories' => [
                    [
                        'id' => $this->getCategory($produto["Grupo"])
                    ]
                ],
            ];
            try {
                $product = $woocommerce->post('products', $data);
                $this->get_image($product, $produto);
                $skuUp = (string)$produto["CodigoPreco"];

                $productId = wc_get_product_id_by_sku($skuUp);

                $data1 = [
                    'name' => $produto["Descricao"],
                    'sku' => $produto["CodigoBarras"],
                    'regular_price' => (string)$produto["Preco"],
                    'manage_stock' => true,
                    'stock_quantity' => (string)$produto["Estoque1"],
                    'attributes' => [
                        [
                            'name' => 'Sabor',
                            'option' => $produto["Complemento"]
                        ]

                    ],

                ];

                try {
                    $product = $woocommerce->post('products/' . $productId . '/variations', $data1);
                    $integra->get_image($product, $produto);
                } catch (Exception $exception) {
                    $message = $exception->getMessage();
                    $skuUp = (string)$produto["CodigoBarras"];
                    $pos = strpos($message, '[product_invalid_sku]');


                    if ($pos !== false) {
                        $variableid = wc_get_product_id_by_sku($skuUp);
                        $data = [
                            'regular_price' => $produto["Preco"],
                            'stock_quantity' => (string)$produto["Estoque1"]
                        ];

                        print_r($woocommerce->put("products/$productId/variations/$variableid", $data));
                    }
                    continue;
                }
            } catch (Exception $e) {
                $message = $e->getMessage();
                $skuUp = (string)$produto["CodigoPreco"];
                $pos = strpos($message, '[product_invalid_sku]');


                if ($pos !== false) {

                    $productId = wc_get_product_id_by_sku($skuUp);

                    $data = [
                        'name' => $produto["Descricao"],
                        'regular_price' => (string)$produto["Preco"],
                        'sku' => $produto["CodigoBarras"],
                        'manage_stock' => true,
                        'stock_quantity' => (string)$produto["Estoque1"],
                        'attributes' => [


                            [
                                'name' => 'Sabor',
                                'option' => $produto["Complemento"],
                            ]

                        ]
                    ];
                    $product = null;
                    try {
                        $product = $woocommerce->post('products/' . $productId . '/variations', $data);
                        $this->get_image($product, $produto);
                    } catch (Exception $exception) {
                        $message = $exception->getMessage();
                        $skuUp = (string)$produto["CodigoBarras"];
                        $pos = strpos($message, '[product_invalid_sku]');


                        if ($pos !== false) {
                            $variableid = wc_get_product_id_by_sku($skuUp);
                            $data = [
                                'regular_price' => (string)$produto["Preco"],
                                'stock_quantity' => (string)$produto["Estoque1"]
                            ];

                            print_r($woocommerce->put("products/$productId/variations/$variableid", $data));
                        }
                        continue;
                    }
                }
                continue;
            }
        }
    }

    public function get_image($product, $produto)
    {
        if (!is_null($product)) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "https://cdn-cosmos.bluesoft.com.br/products/{$produto["CodigoBarras"]}");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');

            $image = curl_exec($ch);
            $responseCode = curl_getinfo($ch)['http_code'];
            curl_close($ch);

            if (200 !== $responseCode) {
                print_r("\n\nImage does not exists. Skipping... (URL) -> https://cdn-cosmos.bluesoft.com.br/products/{$produto["CodigoBarras"]} \n\n");

                return;
            }

            $finfo = new finfo(FILEINFO_MIME_TYPE);
            $mime = $finfo->buffer($image);

            $filename      = (string)$produto["CodigoBarras"] . '.' . str_replace('image/', '', $mime);
            $uploaded_file = wp_upload_bits($filename, null, $image);

            if (!$uploaded_file['error']) {
                $wp_filetype = wp_check_filetype($filename, null);
                $attachment  = array(
                    'post_mime_type' => $wp_filetype['type'],
                    'post_parent'    => $product->id,
                    'post_title'     => preg_replace('/\.[^.]+$/', '', $filename),
                    'post_content'   => '',
                    'post_status'    => 'inherit'
                );

                $attachment_id = wp_insert_attachment($attachment, $uploaded_file['file'], $product->id);

                if (!is_wp_error($attachment_id)) {
                    $attachment_data  = wp_generate_attachment_metadata($attachment_id, $uploaded_file['file']);

                    wp_update_attachment_metadata($attachment_id, $attachment_data);

                    set_post_thumbnail($product->id, $attachment_id);

                    update_post_meta($product->id, '_product_image_gallery', $attachment_id);

                    print_r("\n\nImage created successfully\n\n");
                } else {
                    print($attachment_id->get_error_message() . PHP_EOL);
                    print(' attachment_id is wp error' . PHP_EOL);
                }
            } else {
                print('Error when uploading image: ' . $produto["CodigoBarras"] . ' for product ' . $product->id . PHP_EOL);

                print($uploaded_file['error']);
            }
        }
    }

    function getCategory($codigoGrupo)
    {
        switch ($codigoGrupo) {

            case 10005: {
                    return 20994;
                }
            case 10007: {
                    return 20994;
                }
            case 10008: {
                    return 20994;
                }
            case 10675: {
                    return 20994;
                }
            case 10016: {
                    return 20994;
                }
            case 10013: {
                    return 20994;
                }
            case 10014: {
                    return 20994;
                }
            case 10118: {
                    return 20994;
                }
            case 10026: {
                    return 20994;
                }
            case 10027: {
                    return 20994;
                }
            case 10017: {
                    return 20994;
                }
            case 10018: {
                    return 20994;
                }
            case 10019: {
                    return 20994;
                }
            case 10039: {
                    return 20994;
                }
            case 10041: {
                    return 20994;
                }
            case 10038: {
                    return 20994;
                }
            case 10056: {
                    return 20994;
                }
            case 10058: {
                    return 20994;
                }
            case 10023: {
                    return 20994;
                }
            case 10669: {
                    return 20994;
                }
            case 10057: {
                    return 20994;
                }
            case 10065: {
                    return 20994;
                }
            case 10066: {
                    return 20994;
                }
            case 10064: {
                    return 20994;
                }
            case 10063: {
                    return 20994;
                }
            case 10059: {
                    return 20994;
                }
            case 10061: {
                    return 20994;
                }
            case 10053: {
                    return 20994;
                }
            case 10062: {
                    return 20994;
                }
            case 10050: {
                    return 20994;
                }
            case 10051: {
                    return 20994;
                }
            case 10052: {
                    return 20994;
                }
            case 10045: {
                    return 20994;
                }
            case 10034: {
                    return 20994;
                }
            case 10030: {
                    return 20994;
                }
            case 10031: {
                    return 20994;
                }
            case 10032: {
                    return 20994;
                }
            case 10101: {
                    return 21015;
                }
            case 10102: {
                    return 21015;
                }
            case 10103: {
                    return 21015;
                }
            case 10104: {
                    return 21015;
                }
            case 10105: {
                    return 21015;
                }
            case 10106: {
                    return 21015;
                }
            case 10107: {
                    return 21015;
                }
            case 10126: {
                    return 21015;
                }
            case 10147: {
                    return 21015;
                }
            case 10129: {
                    return 21015;
                }
            case 10130: {
                    return 21015;
                }
            case 10131: {
                    return 21015;
                }
            case 10132: {
                    return 21015;
                }
            case 10134: {
                    return 21015;
                }
            case 10135: {
                    return 21015;
                }
            case 10136: {
                    return 21015;
                }
            case 10677: {
                    return 21015;
                }
            case 10122: {
                    return 21015;
                }
            case 10138: {
                    return 21015;
                }
            case 10139: {
                    return 21015;
                }
            case 10140: {
                    return 21015;
                }
            case 10141: {
                    return 21015;
                }
            case 10142: {
                    return 21015;
                }
            case 10073: {
                    return 21015;
                }
            case 10690: {
                    return 21015;
                }
            case 10113: {
                    return 21015;
                }
            case 10114: {
                    return 21015;
                }
            case 10069: {
                    return 21015;
                }
            case 10071: {
                    return 21015;
                }
            case 10125: {
                    return 21015;
                }
            case 10117: {
                    return 21015;
                }
            case 10124: {
                    return 21015;
                }
            case 10119: {
                    return 21015;
                }
            case 10120: {
                    return 21015;
                }
            case 10248: {
                    return 21015;
                }
            case 10149: {
                    return 21015;
                }
            case 10692: {
                    return 21015;
                }
            case 10256: {
                    return 21015;
                }
            case 10092: {
                    return 21015;
                }
            case 10226: {
                    return 21015;
                }
            case 10209: {
                    return 21015;
                }
            case 10225: {
                    return 21015;
                }
            case 10681: {
                    return 21015;
                }
            case 10204: {
                    return 21015;
                }
            case 10205: {
                    return 21015;
                }
            case 10161: {
                    return 21015;
                }
            case 10162: {
                    return 21015;
                }
            case 10167: {
                    return 21015;
                }
            case 10165: {
                    return 21015;
                }
            case 10164: {
                    return 21015;
                }
            case 10218: {
                    return 201020;
                }
            case 10090: {
                    return 21020;
                }
            case 10689: {
                    return 21020;
                }
            case 10094: {
                    return 21020;
                }
            case 10229: {
                    return 21020;
                }
            case 10085: {
                    return 21020;
                }
            case 10087: {
                    return 21020;
                }
            case 10089: {
                    return 21020;
                }
            case 10088: {
                    return 21020;
                }
            case 10281: {
                    return 21020;
                }
            case 10655: {
                    return 21020;
                }
            case 10283: {
                    return 21020;
                }
            case 10286: {
                    return 21020;
                }
            case 10668: {
                    return 21020;
                }
            case 10293: {
                    return 21020;
                }
            case 10121: {
                    return 21020;
                }
            case 10282: {
                    return 21020;
                }
            case 10287: {
                    return 21020;
                }
            case 10277: {
                    return 21020;
                }
            case 10278: {
                    return 21020;
                }
            case 10273: {
                    return 21020;
                }
            case 10274: {
                    return 21020;
                }
            case 10265: {
                    return 21020;
                }

            case 10303: {
                    return 21086;
                }
            case 10305: {
                    return 21086;
                }
            case 10298: {
                    return 21086;
                }
            case 10304: {
                    return 21086;
                }
            case 10346: {
                    return 21086;
                }
            case 10321: {
                    return 21086;
                }
            case 10326: {
                    return 21086;
                }
            case 10327: {
                    return 21086;
                }
            case 10330: {
                    return 21086;
                }
            case 10332: {
                    return 21086;
                }
            case 10333: {
                    return 21086;
                }
            case 10334: {
                    return 21086;
                }
            case 10620: {
                    return 21086;
                }
            case 10343: {
                    return 21086;
                }
            case 10336: {
                    return 21086;
                }
            case 10362: {
                    return 21086;
                }
            case 10365: {
                    return 21086;
                }
            case 10367: {
                    return 21086;
                }
            case 10318: {
                    return 21086;
                }
            case 10352: {
                    return 21086;
                }
            case 10354: {
                    return 21086;
                }
            case 10355: {
                    return 21086;
                }
            case 10357: {
                    return 21086;
                }
            case 10358: {
                    return 21086;
                }
            case 10309: {
                    return 21027;
                }
            case 10311: {
                    return 21027;
                }
            case 10307: {
                    return 21027;
                }
            case 10096: {
                    return 21027;
                }
            case 10097: {
                    return 21027;
                }
            case 10465: {
                    return 21141;
                }
            case 10459: {
                    return 21141;
                }
            case 10487: {
                    return 21141;
                }
            case 10483: {
                    return 21141;
                }
            case 10485: {
                    return 21141;
                }
            case 10486: {
                    return 21141;
                }
            case 10484: {
                    return 21141;
                }
            case 10490: {
                    return 21141;
                }
            case 10491: {
                    return 21141;
                }
            case 10489: {
                    return 21141;
                }
            case 10481: {
                    return 21141;
                }
            case 10512: {
                    return 21141;
                }
            case 10514: {
                    return 21141;
                }
            case 10513: {
                    return 21141;
                }
            case 10539: {
                    return 21152;
                }
            case 10540: {
                    return 21152;
                }
            case 10541: {
                    return 21152;
                }
            default: {
                    return 21289;
                }
        }
    }
}
