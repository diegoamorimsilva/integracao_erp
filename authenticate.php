<?php

use Automattic\WooCommerce\Client;

/**
 * Autenticação das  APIs: RP Services e WooCommerce.
 */

class authenticate
{
    private $token;
    private $woocommerce;

    /**
     * Dados Necessarios
     *
     * @param string $rp_url         RP Services URL.
     * @param string $user           Usuario RP Services
     * @param string $password       Senha RP Services
     * @param string $woo_url        WooCommercer URL
     * @param string $consumerKey    Consumer key.
     * @param string $consumerSecret Consumer secret.
     */
    public function __construct($rp_url, $user, $password, $woo_url, $consumerKey, $consumerSecret)
    {
        $this->token = $this->rp_authentication($rp_url, $user, $password);
        $this->woocommerce = $this->woo_authentication($woo_url, $consumerKey, $consumerSecret);
    }

    private function rp_authentication(string $url, string $user, string $password)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url . '/v1.1/auth',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => '{
                "usuario": ' . $user . ',
                "senha": ' . $password . '
                }',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        $json_token = json_decode($response, true);
        $token = $json_token["response"]["token"];
       // $token = 'a160a17a-0b18-462b-9621-b19d07da7811';

        return $token;
    }

    private function woo_authentication(string $url, string $consumer_key, string $consumer_secret)
    {
        $woocommerce = new Client(
            $url,
            $consumer_key,
            $consumer_secret,
            [
                'wp_api' => true,
                'version' => 'wc/v3',
                'timeout' => 90000,
                'verify_ssl' => false
            ]
        );

        return $woocommerce;
    }
    /**
     * @return string
     */
    public function getWoocommerce(): Client
    {
        return $this->woocommerce;
    }

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->token;
        //return 'a160a17a-0b18-462b-9621-b19d07da7811';
    }

    public function logout($url){
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url . '/v1.1/logout',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => '{
                "token": '.$this->token.'}',
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json',
            ),
        ));

        curl_exec($curl);

        curl_close($curl);
    }
}
